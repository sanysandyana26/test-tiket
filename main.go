package main

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	_ "github.com/lib/pq"
	"golangTest-1.2/handler"
)

func main() {
	e := echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	// Routes
	e.POST("/tiket/transaction", handler.TransactionTiket)
	e.POST("/tiket/transactions", handler.GetAllTransactions)

	// Start server
	e.Logger.Fatal(e.Start(":1323"))

}
