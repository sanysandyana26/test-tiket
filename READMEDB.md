-- public.m_customer definition

-- Drop table

-- DROP TABLE public.m_customer;

CREATE TABLE public.m_customer (
	idcustomer int8 NOT NULL DEFAULT nextval('m_customer_seq'::regclass),
	nama varchar NULL,
	email varchar NULL,
	CONSTRAINT m_customer_pk PRIMARY KEY (idcustomer)
);


-- public.m_tiket definition

-- Drop table

-- DROP TABLE public.m_tiket;

CREATE TABLE public.m_tiket (
	idtiket int8 NOT NULL DEFAULT nextval('m_tiket_seq'::regclass),
	namatiket varchar NOT NULL,
	kodetiket varchar NULL,
	stoktiket int8 NOT NULL,
	CONSTRAINT m_tiket_pk PRIMARY KEY (idtiket)
);


-- public.t_transaction definition

-- Drop table

-- DROP TABLE public.t_transaction;

CREATE TABLE public.t_transaction (
	idtransaction int8 NOT NULL DEFAULT nextval('t_transaction_seq'::regclass),
	idtiket int8 NULL,
	idcustomer int8 NULL,
	jumlahtiket varchar NULL,
	CONSTRAINT t_transaction_pk PRIMARY KEY (idtransaction),
	CONSTRAINT t_transaction_fk FOREIGN KEY (idtiket) REFERENCES public.m_tiket(idtiket),
	CONSTRAINT t_transaction_fk_1 FOREIGN KEY (idcustomer) REFERENCES public.m_customer(idcustomer)
);
