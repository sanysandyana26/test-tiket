package handler

import (
	"fmt"
	"github.com/labstack/gommon/log"
	_ "github.com/lib/pq"
	"golangTest-1.2/midlleware"
	"golangTest-1.2/model"
)

func InsertTrasactionr(u model.Transaction) int {
	//var id int
	db := dbConnection.CreateConnection()
	defer db.Close()

	fmt.Println(u.IdTiket)
	fmt.Println(u.JumlahTiket)

	stmt, err := db.Prepare("INSERT INTO public.t_transaction (idtiket, idcustomer, jumlahtiket) VALUES($1, $2, $3)")
	if err != nil {
		log.Fatal(err)
		return 1
	}
	defer stmt.Close()
	_, err2 := stmt.Exec(u.IdTiket, u.IdCustomer, u.JumlahTiket)
	if err2 != nil {
		log.Fatal(err)
		return 1
	}

	t := model.Tiket{
		ID:         u.IdTiket,
		StockTiket: u.JumlahTiket,
	}

	err3 := UpdateStockTiketMinus(t)
	if err3 != 0 {
		log.Fatal(err)
		return 1
	}
	fmt.Println("insert success!")

	return 0
}

func UpdateStockTiketMinus(u model.Tiket) int {
	db := dbConnection.CreateConnection()
	defer db.Close()

	log.Info(u.ID)
	log.Info(u.StockTiket)
	stmt, err := db.Prepare("UPDATE public.m_tiket SET stoktiket=(stoktiket - $2) WHERE idtiket = $1")
	if err != nil {
		log.Fatal(err)
		return 1
	}
	defer stmt.Close()
	_, err2 := stmt.Exec(u.ID, u.StockTiket)
	if err2 != nil {
		log.Fatal(err)
		return 1
	}
	fmt.Println("success!")

	return 0
}

func GetTiketByID(u model.Tiket) (model.Tiket, int) {
	db := dbConnection.CreateConnection()
	defer db.Close()

	var result = model.Tiket{}
	err := db.
		QueryRow("SELECT idtiket, namatiket, kodetiket, stoktiket FROM public.m_tiket WHERE idtiket=$1", u.ID).
		Scan(&result.ID, &result.NameTiket, &result.CodeTiket, &result.StockTiket)
	if err != nil {
		fmt.Println(err.Error())
		return result, 1
	}

	fmt.Printf("name: %s\n mail: %d\n", result.ID, result.NameTiket)

	return result, 0
}

func GetAllTransaction() ([]model.Transaction, int) {
	db := dbConnection.CreateConnection()
	defer db.Close()

	var result []model.Transaction
	rows, err := db.Query("SELECT idtransaction, idtiket, idcustomer, jumlahtiket FROM public.t_transaction  ORDER BY idtransaction DESC ")
	if err != nil {
		fmt.Println(err.Error())
		return result, 1
	}
	defer rows.Close()

	for rows.Next() {
		var each = model.Transaction{}
		var err = rows.Scan(&each.ID, &each.IdTiket, &each.IdCustomer, &each.JumlahTiket)

		if err != nil {
			fmt.Println(err.Error())
			return result, 1
		}

		result = append(result, each)
	}

	if err = rows.Err(); err != nil {
		fmt.Println(err.Error())
		return result, 1
	}

	for _, each := range result {
		fmt.Println(each.ID)
	}
	return result, 0

}
