package handler

import (
	"fmt"
	"github.com/labstack/echo/v4"
	_ "github.com/lib/pq"
	"golangTest-1.2/model"
	"net/http"
	"sync"
)

var (
	lock = sync.Mutex{}
)

const (
	InteranlError  = "Internal Server Error !!"
	DataNotFount   = "Data Not Fount !!"
	MailRehistered = "Mail Registered !!"
)

func TransactionTiket(c echo.Context) error {
	lock.Lock()
	defer lock.Unlock()
	u := &model.Transaction{}
	if err := c.Bind(u); err != nil {
		return err
	}

	fmt.Println(u.IdTiket)
	fmt.Println(u.JumlahTiket)
	t := model.Tiket{
		ID: u.IdTiket,
	}
	b, err1 := GetTiketByID(t)
	if err1 != 0 {
		return c.JSON(http.StatusOK, model.Response{Description: InteranlError, Data: []interface{}{}})
	}

	if b.StockTiket == 0 {
		return c.JSON(http.StatusOK, model.Response{Description: "stock not ready !!", Data: []interface{}{}})
	}

	a := InsertTrasactionr(*u)
	if a != 0 {
		return c.JSON(http.StatusOK, model.Response{Description: InteranlError, Data: []interface{}{}})
	}
	return c.JSON(http.StatusCreated, model.Response{Description: "Success", Data: []interface{}{u}})
}

func GetAllTransactions(c echo.Context) error {
	lock.Lock()
	defer lock.Unlock()
	u := &model.Transaction{}
	if err := c.Bind(u); err != nil {
		return err
	}

	a, i := GetAllTransaction()
	if i == 1 {
		return c.JSON(http.StatusOK, model.Response{Description: DataNotFount, Data: []interface{}{}})
	}
	return c.JSON(http.StatusOK, model.ResponseList{Description: "Success", Data: a})
}
